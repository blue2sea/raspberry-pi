import RPi.GPIO as GPIO
import time

MIN_DUTY = 3
MAX_DUTY = 11
CENTRE = MIN_DUTY + (MAX_DUTY - MIN_DUTY) / 2

servo_pin = 18  # pwm pin - GPIO 18
duty_cycle = CENTRE  # Should be the centre for a SG90

# Configure the Pi to use pin names (i.e. BCM) and allocate I/O
GPIO.setmode(GPIO.BCM)
GPIO.setup(servo_pin, GPIO.OUT)

# Create PWM channel on the servo pin with a frequency of 50Hz
pwm_servo = GPIO.PWM(servo_pin, 50)
pwm_servo.start(duty_cycle)

try:
    while True:
        # rotate forward
        for i in range(0, (MAX_DUTY - MIN_DUTY) * 40):
            pwm_servo.ChangeDutyCycle(MIN_DUTY + i / 40.0)
            time.sleep(0.05)

        # rotate backward
        for i in range(0, (MAX_DUTY - MIN_DUTY) * 40):
            pwm_servo.ChangeDutyCycle(MAX_DUTY - i / 40.0)
            time.sleep(0.05)

except KeyboardInterrupt:
    print("CTRL-C: Terminating program.")
finally:
    print("Cleaning up GPIO...")
    pwm_servo.ChangeDutyCycle(CENTRE)
    time.sleep(0.5)
    GPIO.cleanup()
