import os
import logging.config
import ConfigParser

# current path
cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

# read ini file
try:
    config = ConfigParser.ConfigParser()
    config.read(cur_dir + 'global.ini')
except IOError:
    print 'Failed to load ini file'


class Streamer:
    logger = None
    sn = ''
    stream_type = ''

    def __init__(self, main_logger, sn, stream_type):
        self.logger = main_logger
        self.sn = sn
        self.stream_type = stream_type

    def run(self):
        pipeline = ''
        if self.stream_type == 'IP':
            pipeline = 'gst-launch-1.0 rtspsrc location=rtsp://192.168.1.160:8554/0 ! rtph264depay ! flvmux ! rtmpsink location=rtmp://stream_url/live/stream_id'
        if self.stream_type == 'THERMAL':
            pipeline = 'gst-launch-1.0 -v v4l2src ! video/x-raw,width=320,height=240 ! omxh264enc target-bitrate=2097152 control-rate=variable ! video/x-h264,width=320,height=240 ! h264parse ! flvmux ! rtmpsink location=rtmp://stream_url/live/stream_id'
        pipeline = pipeline.replace('stream_url', config.get('URL', 'STREAM_URL'))
        pipeline = pipeline.replace('stream_id', self.sn + '_' + self.stream_type)

        try:
            os.system(pipeline)
        except Exception as e:
            self.logger.error('%s in streaming', e)


if __name__ == '__main__':
    cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'
    # set logger
    try:
        logging.config.fileConfig(cur_dir + 'logging.conf')
    except IOError:
        print 'Failed to load configuration file'
    logger = logging.getLogger('streamer')
    test_streamer = Streamer(logger, '0001', 'THERMAL')
    test_streamer.run()

